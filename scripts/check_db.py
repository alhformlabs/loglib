from loglib.factory import get_db_logger, DBConfig
import logging

conf = DBConfig("127.0.0.1", "5432", "quik", "error", 'postgres', 'postgres')
logger = get_db_logger(conf, level=logging.INFO)
logger.info({"error": "some_dummy_error", "sender": "alex", "sender_id": 22})