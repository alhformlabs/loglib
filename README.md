## Small package for custom logging. 

Supports:
* Slack
* PostgreSQL database

Requirements to input: you should pass dict of values. Fully compatibale

## Sample usage

* Postgres
Input =  dict[db_column_name:value]
```
from loglib.factory import get_db_logger, DBConfig
import logging

conf = DBConfig("127.0.0.1", "5432", "quik", "error", 'postgres', 'postgres')
logger = get_db_logger(conf, level=logging.INFO)
logger.info({"error": "some_dummy_error", "sender": "alex", "sender_id": 22})
```

* Slack
Input = dict of Slack API payload
```
import logging

from loglib.factory import get_slack_logger

url = "https://hooks.slack.com/services/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"
logger = get_slack_logger(url, level=logging.INFO)
logger.info({"text": "Hello world"})
```

If you want to format your messages in any specific way:

1. Subclass one of existing handlers
2. Overwrite format method