import logging
from collections import namedtuple

import psycopg2

DBFormat = namedtuple("DBFormat", ("sql", "values"))
DBConfig = namedtuple("DBConfig", ("host", "port", "database", "table", "user", "password"))


class PostgresHandler(logging.Handler):

    def __init__(self, db_config: DBConfig, level=logging.WARNING):
        self.port = db_config.port
        self.host = db_config.host
        self.user = db_config.user
        self.password = db_config.password
        self.dbname = db_config.database
        self.table = db_config.table
        self.cursor = None
        super().__init__(level)

    def emit(self, record):
        with psycopg2.connect(user=self.user, password=self.password,
                              host=self.host, port=self.port, database=self.dbname) as conn:
            with conn.cursor() as cur:
                message = self.format(record)
                cur.execute(message.sql, message.values)

    def format(self, record) -> DBFormat:
        columns_list = []
        values = []

        for k, v in record.msg.items():
            columns_list.append(k)
            values.append(v)

        columns = '(' + ','.join(columns_list) + ')'
        parameters = '(' + ','.join(['%s' for i in columns_list]) + ')'

        sql = """INSERT INTO {} {} VALUES {}""".format(self.table, columns, parameters)
        values = tuple([j for j in values])
        return DBFormat(sql, values)
