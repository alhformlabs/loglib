import json
import logging

import requests


class SlackLogHandler(logging.Handler):

    def __init__(self, slack_url, level=logging.WARNING):
        self._slack_url = slack_url
        super().__init__(level)

    def emit(self, record):
        message = self.format(record)
        resp = requests.post(self._slack_url, json=message)
        try:
            resp.raise_for_status()
        except requests.HTTPError as e:
            logging.exception(f"Failed to post in Slack. HTTP code: {resp.status_code}. JSON: {resp.json()}")
            raise e
        return resp

    def format(self, record):
        if isinstance(record.msg, dict):
            return record.msg
        return json.loads(record.msg)
