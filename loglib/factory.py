import logging

from .handlers.db import PostgresHandler, DBConfig
from .handlers.slack import SlackLogHandler


def get_slack_logger(url, level=logging.WARNING) -> logging:
    logger = logging.getLogger("SLACK")
    handler = SlackLogHandler(url, level)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger


def get_db_logger(db_config: DBConfig, level=logging.WARNING) -> logging:
    logger = logging.getLogger("POSTGRES")
    logger.setLevel(level)
    handler = PostgresHandler(db_config, level)
    logger.addHandler(handler)
    return logger


def get_custom_logger(handler: logging.Handler, level, name='CUSTOM'):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger
